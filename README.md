# README #

Place an order with a customer address that has a non-empty company value and verify the sales order grid in the backend has the company field

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/CompanyName when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade