<?php

namespace Mbs\CompanyName\Model\ResourceModel\Order\Customer;

class Collection extends \Magento\Sales\Model\ResourceModel\Order\Customer\Collection
{
    /**
     * @return \Mbs\CompanyName\Model\ResourceModel\Order\Customer\Collection
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->joinAttribute(
            'billing_company',
            'customer_address/company',
            'default_billing',
            null,
            'left'
        );

        return $this;
    }
}